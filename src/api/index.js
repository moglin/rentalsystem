import http from './public'
// 登陆
export const userLogin = (fd) => {
  return http.fetchPost('/api/user/login', fd);
};
export const changeState = (params) => {
  return http.fetchPost('api/admin/home/updateHomeStatus', params)
}
// 退出登陆
export const logOut = () => {
  return http.fetchPost('/api/user/logout')
};
// 获取个人信息
export const myInfo = () => {
  return http.fetchGet('/api/user/getMyInfo')
};
// 获取用户信息
export const getUserInfo = (uid) => {
  return http.fetchGet('/api/user/getUserInfo/'+uid)
};
export const getAllUsers = () => {
  return http.fetchGet('/api/admin/getAllUser');
}
// 修改个人信息
export const changeUserInfo = (params) => {
  return http.fetchPost('/api/user/changeUserInfo', params)
};
//管理员修改个人信息
export const adminChangeUserInfo = (params) => {
  return http.fetchPost('/api/admin/changeUserInfo', params)
};
// 注册账号
export const userRegister = (params) => {
  return http.fetchPost('/api/user/register', params)
};
export const getVerificationCode = (params) => {
  return http.fetchPost('/api/user/getVeryCode',params)
}
// 注册多种账号
export const workerRegister = (params) => {
  return http.fetchPost('/api/admin/addUser', params)
};
// 获取所有订单信息
export const userOrders = () => {
  return http.fetchGet('/api/application/getAll')
};
// 获取订单信息
export const getOrder = (aid) => {
  return http.fetchGet('api/application/getById/' + aid)
};
// 检查登录状态
export const checkCookie = () => {
  return http.fetchGet('/author')
};
//提交房源信息
export const addHome = (params) => {
  return http.fetchPost('/api/admin/home/addHome', params)
};
// 上传图片
export const upload = (params) => {
  return http.fetchPost('api/img/uploadImage', params)
};
// 上传复数张图片
export const mulitUpload = (params) => {
  return http.fetchPost('api/img/uploadImages', params)
};
// 房源搜索
export const searchH = (params) => {
  return http.fetchGet('api/home/search', params)
};
export const searchAll = () => {
  return http.fetchGet('api/home/search?kw=');
};
// 提交工单
export const submitFeedback = (params) => {
  return http.fetchPost('/api/repair/add', params)
};
// 用户获取所有工单
export const userGetWorkOrder = () => {
  return http.fetchGet('/api/repair/getAll')
};
// 工人获取所有工单
export const workerGetWorkOrder = () => {
  return http.fetchGet('/api/worker/getAllRepair')
};
// 管理员获取所有工单
export const adminGetWorkOrder = () => {
  return http.fetchGet('/api/admin/home/getAllRepair')
};
// 获取工单的额外信息
export const getFeedbackExtra = (params) => {
  return http.fetchPost('/api/repair/getExtra',params)
};
// 指定工单处理师傅
export const assignWorker = (params) => {
  return http.fetchPost('/api/admin/home/assignWorker/',params)
};
// 获取房源信息
export const getHomeInfo = (hid) => {
  return http.fetchGet('api/home/getHomeInfo/'+ hid)
};
// 租房
export const rentHouse = (params) => {
  return http.fetchPost('/api/home/rent', params)
};
// 短租付款
export const userPay = (params) => {
  return http.fetchPost('/api/home/pay', params)
};
// 获取单个工单信息 rid: repair id
export const getRepairDetail = (params) => {
  return http.fetchPost('/api/repair/details', params)
};
// 获取所有待处理的订单
export const getApplicationToSolve = () => {
  return  http.fetchGet('/api/admin/home/getApplicationToSolve')
};
// 更改订单状态
export const updateApplicationStatus = (params) => {
  return  http.fetchPost('api/admin/home/updateApplicationStatus',params)
};
// 获取师傅的列表
export const getAllWorker = (params) => {
  return http.fetchGet('/api/admin/getAllWorker', params)
};
// 用户提交工单回复
export const userReplyRepair = (params) => {
  return http.fetchPost('/api/repair/message', params)
};
// 工人提交工单回复
export const workerReplyRepair = (params) => {
  return http.fetchPost('/api/worker/addMessage', params)
};
// 管理员提交工单回复
export const adminReplyRepair = (params) => {
  return http.fetchPost('/api/admin/home/addMessage', params)
};
// 用户关闭工单
export const userCloseRepair = (params) => {
  return http.fetchPost('/api/repair/close', params)
};
// 管理员关闭工单
export const adminCloseRepair = (params) => {
  return http.fetchPost('/api/admin/home/close', params)
};
// 用户提交评价
export const userSubmitJudge = (params) => {
  return http.fetchPost('/api/repair/rate', params)
};
/*
// 极验验证码
export const geetest = (params) => {
  return http.fetchGet('/member/geetestInit?t=' + (new Date()).getTime(), params)
} */

