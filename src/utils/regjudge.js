export const checkPhoneNum = (phoneNum) => {
    return /^1[34578]\d{9}$/.test(phoneNum);
};
