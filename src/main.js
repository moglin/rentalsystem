/*!

=========================================================
* Vue Argon Design System - v1.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/argon-design-system
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/argon-design-system/blob/master/LICENSE.md)

* Coded by www.creative-tim.com

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import Argon from "./plugins/argon-kit";
import BootstrapVue from 'bootstrap-vue';
import axios from 'axios';
import './registerServiceWorker'
// import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import VueDirectiveImagePreviewer from 'vue-directive-image-previewer'
import 'vue-directive-image-previewer/dist/assets/style.css'
import global_ from './Global.vue'

Vue.config.productionTip = false;
Vue.prototype.$http = axios;
Vue.prototype.GLOBAL = global_;
axios.defaults.withCredentials = true;
// axios.defaults.baseURL='http://10.128.26.163:8080/';  //base_url请求路径-临时环境
axios.defaults.baseURL= global_.BASE_URL;  //base_url请求路径-华为云
// axios 全局拦截：未登录
axios.interceptors.response.use(
    response => {
      // console.log(response.code);
      if (response.data.code === "110" || response.data.code === "111"){router.push({path:'/login'})}
      return response;
    },
    error => {
      return Promise.reject(error.response.data)
    }
);

Vue.use(Argon);
Vue.use(BootstrapVue);
Vue.use(VueDirectiveImagePreviewer);
new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
