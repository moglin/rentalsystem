import Vue from "vue";
import Router from "vue-router";
import AppHeader from "./layout/AppHeader";
import AppFooter from "./layout/AppFooter";
import Components from "./views/Components.vue";
import Landing from "./views/Landing.vue";
import Login from "./views/Login.vue";
import Register from "./views/Register.vue";
import Profile from "./views/Profile.vue";
import Search from "./views/Search.vue";
import House from "./views/House.vue"
import CheckManage from "./views/CheckManage.vue"
import AddNewHouse from "./views/AddNewHouse.vue"
import Feedback from "./views/Feedback.vue"
import WorkOrderManage from "./views/WorkOrderManage.vue"
import WorkOrder from "./views/WorkOrder.vue"
import Index from "./views/Index.vue"
import CheckAudit from "./views/CheckAudit.vue"
import WorkerRegister from "./views/WorkerRegister.vue"
import CustomServiceIndex from "./views/CustomServiceIndex.vue"
import HouseList from "./views/HouseList.vue"
import ContractManage from "./views/ContractManage.vue"
import UserList from "./views/UserList.vue"
import Contract from "./views/Contract.vue"
import Test from "./views/Test.vue"
import Rent from "./views/Rent";

Vue.use(Router);

export default new Router({
  linkExactActiveClass: "active",
  mode: "hash",
  routes: [
    {
      path: "/",
      name: "index",
      components: {
        header: AppHeader,
        default: Index,
        footer: AppFooter
      }
    },
    {
      path: "/components",
      name: "components",
      components: {
        header: AppHeader,
        default: Components,
        footer: AppFooter
      }
    },
    {
      path: "/landing",
      name: "landing",
      components: {
        header: AppHeader,
        default: Landing,
        footer: AppFooter
      }
    },
    {
      path: "/login",
      name: "login",
      components: {
        header: AppHeader,
        default: Login,
        footer: AppFooter
      }
    },
    {
      path: "/register",
      name: "register",
      components: {
        header: AppHeader,
        default: Register,
        footer: AppFooter
      }
    },
    {
      path: "/profile/:uid?",
      name: "profile",
      components: {
        header: AppHeader,
        default: Profile,
        footer: AppFooter
      }
    },
    {
      path: "/search/:kw?",
      name: "search",
      components: {
        header: AppHeader,
        default: Search,
        footer: AppFooter
      }
    },
    {
      path: "/house/:hid?",
      name: "house",
      components: {
        header: AppHeader,
        default: House,
        footer: AppFooter
      }
    },
    {
      path: "/check-manage",
      name: "check-manage",
      components: {
        header: AppHeader,
        default: CheckManage,
        footer: AppFooter
      }
    },
    {
      path: "/test",
      name: "test",
      components: {
        default: Test,
      }
    },
    {
      path: "/rent/:id?",
      name: "rent",
      components: {
        header: AppHeader,
        default: Rent,
        footer: AppFooter
      }
    },
    {
      path: "/add-new-house",
      name: "add-new-house",
      components: {
        header: AppHeader,
        default: AddNewHouse,
        footer: AppFooter
      }
    },
    {
      path: "/feedback/:id?",
      name: "feedback",
      components: {
        header: AppHeader,
        default: Feedback,
        footer: AppFooter
      }
    },
    {
      path: "/work-order-manage",
      name: "work-order-manage",
      components: {
        header: AppHeader,
        default: WorkOrderManage,
        footer: AppFooter
      }
    },
    {
      path: "/work-order/:rid?",
      name: "work-order",
      components: {
        header: AppHeader,
        default: WorkOrder,
        footer: AppFooter
      }
    },
    {
      path: "/check-audit",
      name: "check-audit",
      components: {
        header: AppHeader,
        default: CheckAudit,
        footer: AppFooter
      }
    },
    {
      path: "/worker-register",
      name: "worker-register",
      components: {
        header: AppHeader,
        default: WorkerRegister,
        footer: AppFooter
      }
    },
    {
      path: "/custom-service-index",
      name: "custom-service-index",
      components: {
        header: AppHeader,
        default: CustomServiceIndex,
        footer: AppFooter
      }
    },
    {
      path: "/house-list",
      name: "house-list",
      components: {
        header: AppHeader,
        default: HouseList,
        footer: AppFooter
      }
    },
    {
      path: "/contract-manage",
      name: "contract-manage",
      components: {
        header: AppHeader,
        default: ContractManage,
        footer: AppFooter
      }
    },
    {
      path: "/user-list",
      name: "user-list",
      components: {
        header: AppHeader,
        default: UserList,
        footer: AppFooter
      }
    },
    {
      path: "/contract",
      name: "contract",
      components: {
        default: Contract,
      }
    },
  ],
  scrollBehavior: to => {
    if (to.hash) {
      return { selector: to.hash };
    } else {
      return { x: 0, y: 0 };
    }
  }
});
